#ifndef _IVM_VM_NATIVE_LIST_H_
#define _IVM_VM_NATIVE_LIST_H_

#include "pub/com.h"
#include "pub/type.h"
#include "pub/vm.h"

IVM_COM_HEADER

IVM_NATIVE_FUNC(_list_size);
IVM_NATIVE_FUNC(_list_slice);

IVM_COM_END

#endif
