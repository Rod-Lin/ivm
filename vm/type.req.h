#ifndef _IVM_VM_TYPE_REQ_H_
#define _IVM_VM_TYPE_REQ_H_

#include "pub/inlines.h"

#include "native/native.h"

#include "obj.h"
#include "num.h"
#include "strobj.h"
#include "func.h"
#include "listobj.h"

#endif
